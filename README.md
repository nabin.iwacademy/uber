## Resolvers

### Public Resolvers:

- [x] Sign In and Sign Up with facebook
- [x] Sign In with email
- [x] Start Phone Number verification
- [x] Complete phone number verification
- [x] Sign Up with Email

---

### Authentication

- [x] Generate JWT
- [x] verify JWT

---

### Private Resolvers:

- [x] Get My Profile
- [x] Request Email Verification
- [x] Complete Email Verification
- [ ] Update My Profile
- [ ] Report location/ Orientation
- [ ] Add Place
- [ ] Edit Place
- [ ] Edit Place
- [ ] Delete Place
- [ ] See nearby Drivers
- [ ] Subscribe to Nearby Drivers
- [ ] Request the Ride
- [ ] Get nearby ride
- [ ] Subscribe to nearby ride request
- [ ] Subscribe to Ride Requests
- [ ] Subscribe to Chat Room Messages
- [ ] Send A Char Message

## Coding Challenges

- [ ] Get Ride History
- [ ] Get Ride Details
