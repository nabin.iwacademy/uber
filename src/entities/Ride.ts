import { rideStatus } from "./../types/types.d";
import {
  BaseEntity,
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from "typeorm";
import User from "./User";
@Entity()
class Place extends BaseEntity {
  @PrimaryGeneratedColumn() id: number;
  @Column({
    type: "text",
    enum: ["ACCEPTED", "FINISHED", "CANCELED", "REQUESTING", "ONROUTE"],
  })
  status: rideStatus;
  @Column({ type: "text" })
  pickUpAddress: string;
  @Column({ type: "double precision", default: 0 })
  pickUpLat: number;
  @Column({ type: "double precision", default: 0 })
  pickUpLng: number;
  @Column({ type: "text" })
  dropOffAddress: string;
  @Column({ type: "double precision", default: 0 })
  dropOffLat: number;
  @Column({ type: "double precision", default: 0 })
  dropOffLng: number;
  @Column({ type: "double precision", default: 0 })
  price: number;
  @Column({ type: "text" })
  distance: string;
  @CreateDateColumn()
  createdAt: string;
  @ManyToOne((type) => User, (user) => user.ridesAsPassenger)
  passenger: User;
  @ManyToOne((type) => User, (user) => user.ridesAsDriver)
  driver: User;

  @UpdateDateColumn()
  updatedAt: string;
}

export default Place;
