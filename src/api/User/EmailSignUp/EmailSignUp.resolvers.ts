import User from "../../../entities/User";
import {
  EmailSignUpResponse,
  EmailSignUpMutationArgs,
} from "./../../../types/graph.d";
import { Resolvers } from "./../../../types/resolvers.d";
import createJWT from "../../../utils/createJWT";
import Verification from "../../../entities/Verification";
import { sendVerificationEmail } from "../../../utils/sendEmail";
const resolvers: Resolvers = {
  Mutation: {
    EmailSignUp: async (
      _,
      args: EmailSignUpMutationArgs
    ): Promise<EmailSignUpResponse> => {
      const { email } = args;
      try {
        const user = await User.findOne({ email });

        if (user) {
          return {
            ok: false,
            error: " You Should Sign in",
            token: null,
          };
        } else {
          const phoneVerification = await Verification.findOne({
            payload: args.phoneNumber,
            verified: true,
          });
          if (phoneVerification) {
            const newUser = await User.create({ ...args }).save();
            if (newUser.email) {
              const emailVerification = await Verification.create({
                payload: newUser.email,
                target: "EMAIL",
              }).save();
              await sendVerificationEmail(
                newUser.fullName,
                emailVerification.key
              );
            }

            const token = createJWT(newUser.id);

            return {
              ok: true,
              error: null,
              token,
            };
          } else {
            return {
              ok: false,
              error: "You haven't verified your phone yet",
              token: null,
            };
          }
        }
      } catch (error) {
        return {
          ok: false,
          error: error.message,
          token: null,
        };
      }
    },
  },
};

export default resolvers;
