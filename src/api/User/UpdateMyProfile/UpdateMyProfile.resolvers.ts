import { UpdateMyProfileMutationArgs } from "./../../../types/graph.d";

import { Resolvers } from "./../../../types/resolvers.d";
import privateResolver from "../../../utils/privateResolver";
const resolvers: Resolvers = {
  Mutation: {
    GetMyProfile: privateResolver(
      async (_, args: UpdateMyProfileMutationArgs, { req }) => {}
    ),
  },
};
export default resolvers;
