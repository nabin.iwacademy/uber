// import Mailgun from "mailgun-js";
const sgMail = require("@sendgrid/mail");

sgMail.setApiKey(process.env.SENDGRID_API_KEY || "");

const sendEmail = (subject: string, html: string, text: string) => {
  const emailData = {
    from: "shresthanabin94@gmail.com",
    to: "shresthanabin94@gmail.com",
    subject,
    text,
    html,
  };
  return sgMail.send(emailData);
};

export const sendVerificationEmail = (fullName: string, key: string) => {
  const emailSubject = `Hello ${fullName}, please verify your email.`;
  const emailBody = `Verify your email by clicking <a href="http://pythonreal.co/verification/${key}/">Here</a>`;
  const emailText = "hello this is your verification";
  return sendEmail(emailSubject, emailBody, emailText);
};
